package ru.mikhaylov.testlight;

import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.responses.JwtResponse;
import ru.mikhaylov.testlight.dtos.responses.TokenResponse;
import ru.mikhaylov.testlight.jwt.JwtProvider;
import ru.mikhaylov.testlight.repos.UserRepo;
import ru.mikhaylov.testlight.services.AuthService;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class AuthServiceTest {

    private AuthService authService;
    private JwtProvider jwtProvider;
    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        jwtProvider = mock(JwtProvider.class);
        userRepo = mock(UserRepo.class);
        passwordEncoder = mock(PasswordEncoder.class);
        authService = new AuthService(jwtProvider, userRepo, passwordEncoder);
    }

    @Test
    public void testAuthByPassword_Success() {
        String login = "testuser";
        String password = "password";
        User user = new User();
        user.setId(1L);
        user.setPassword(passwordEncoder.encode(password));
        when(userRepo.findByLogin(login)).thenReturn(java.util.Optional.of(user));
        when(passwordEncoder.matches(password, user.getPassword())).thenReturn(true);

        TokenResponse tokenResponse = new TokenResponse("abcd", Instant.now().plus(30, TimeUnit.DAYS.toChronoUnit()));
        when(jwtProvider.generateRefreshToken(user)).thenReturn(tokenResponse);
        when(jwtProvider.generateAccessToken(user)).thenReturn(tokenResponse);

        JwtResponse response = authService.authByPassword(login, password);

        assertNotNull(response);
        assertNotNull(response.getAccessToken());
        assertNotNull(response.getRefreshToken());
        verify(userRepo, times(1)).findByLogin(login);
        verify(passwordEncoder, times(1)).matches(password, user.getPassword());
    }

    @Test
    public void testAuthByPassword_InvalidPassword() {

        String login = "testuser";
        String password = "password";
        User user = new User();
        user.setId(1L);
        user.setPassword(passwordEncoder.encode(password));
        when(userRepo.findByLogin(login)).thenReturn(java.util.Optional.of(user));
        when(passwordEncoder.matches(password, user.getPassword())).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> authService.authByPassword(login, password));
        verify(userRepo, times(1)).findByLogin(login);
        verify(passwordEncoder, times(1)).matches(password, user.getPassword());
    }

    @Test
    public void testLogout() {
        long id = 1L;
        authService.logout(id);
        verify(userRepo, times(1)).deleteAllById(id);
    }
}
