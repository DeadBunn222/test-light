package ru.mikhaylov.testlight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.domain.Role;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.responses.UserResponse;
import ru.mikhaylov.testlight.repos.UserRepo;
import ru.mikhaylov.testlight.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserService userService;
    private UserRepo userRepo;
    private ModelMapper modelMapper;

    @BeforeEach
    public void setUp() {
        userRepo = mock(UserRepo.class);
        modelMapper = new ModelMapper();
        userService = new UserService(userRepo, modelMapper);
    }

    @Test
    public void testFindById_UserExists() {
        long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        User result = userService.findById(userId);

        assertNotNull(result);
        assertEquals(userId, result.getId());
    }

    @Test
    public void testFindById_UserNotExists() {
        long userId = 1L;
        when(userRepo.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.findById(userId));
    }

    @Test
    public void testGetUsersList() {
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        userList.add(new User());
        when(userRepo.findAll()).thenReturn(userList);

        List<UserResponse> result = userService.getUsersList();

        assertNotNull(result);
        assertEquals(userList.size(), result.size());
    }

    @Test
    public void testSetOperator() {
        long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        UserResponse result = userService.setOperator(userId);

        assertNotNull(result);
        assertTrue(user.getRoles().contains(Role.OPERATOR));
    }
}
