package ru.mikhaylov.testlight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.dadata.PhoneValidationService;
import ru.mikhaylov.testlight.domain.Request;
import ru.mikhaylov.testlight.domain.RequestType;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.requests.CreateRequest;
import ru.mikhaylov.testlight.dtos.responses.RequestResponse;
import ru.mikhaylov.testlight.repos.RequestRepo;
import ru.mikhaylov.testlight.services.RequestService;
import ru.mikhaylov.testlight.services.UserService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RequestServiceTest {

    private RequestService requestService;
    private UserService userService;
    private RequestRepo requestRepo;
    private ModelMapper modelMapper;
    private PhoneValidationService phoneValidationService;

    @BeforeEach
    public void setUp() {
        userService = mock(UserService.class);
        requestRepo = mock(RequestRepo.class);
        modelMapper = new ModelMapper();
        phoneValidationService = mock(PhoneValidationService.class);
        requestService = new RequestService(userService, requestRepo, modelMapper, phoneValidationService);
    }

    @Test
    public void testFindById_RequestExists() {
        long requestId = 1L;
        Request request = new Request();
        request.setId(requestId);
        when(requestRepo.findById(requestId)).thenReturn(Optional.of(request));

        Request result = requestService.findById(requestId);

        assertNotNull(result);
        assertEquals(requestId, result.getId());
    }

    @Test
    public void testFindById_RequestNotExists() {
        long requestId = 1L;
        when(requestRepo.findById(requestId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> requestService.findById(requestId));
    }

    @Test
    public void testCreateAndSendRequest_Success() {
        long userId = 1L;
        CreateRequest createRequest = new CreateRequest();
        Request request = new Request();
        when(userService.findById(userId)).thenReturn(new User());
        when(requestRepo.save(any(Request.class))).thenReturn(request);
        RequestResponse result = requestService.createAndSendRequest(createRequest, userId);

        assertNotNull(result);
        verify(requestRepo, times(1)).save(any(Request.class));
    }

    @Test
    public void testCreateDraftRequest_Success() {
        long userId = 1L;
        CreateRequest createRequest = new CreateRequest();
        Request request = new Request();
        when(userService.findById(userId)).thenReturn(new User());
        when(requestRepo.save(any(Request.class))).thenReturn(request);
        RequestResponse result = requestService.createDraftRequest(createRequest, userId);

        assertNotNull(result);
        verify(requestRepo, times(1)).save(any(Request.class));
    }

    @Test
    public void testEditRequest_Success() {

        long requestId = 1L;
        long userId = 2L;
        User user = new User();
        user.setId(userId);
        CreateRequest createRequest = new CreateRequest();
        createRequest.setPhoneNumber("diff_phone_number");
        Request request = new Request();
        request.setId(requestId);
        request.setCreator(user);
        request.setType(RequestType.DRAFT);
        request.setPhoneNumber("123");
        when(requestRepo.findById(requestId)).thenReturn(Optional.of(request));
        when(requestRepo.save(request)).thenReturn(request);
        RequestResponse result = requestService.editRequest(requestId, userId, createRequest);

        assertNotNull(result);
        assertEquals(requestId, result.getId());
        verify(phoneValidationService, times(1)).validatePhoneNumber(createRequest.getPhoneNumber());
    }

    @Test
    public void testEditRequest_Forbidden() {

        long requestId = 1L;
        long userId = 2L;
        User user = new User();
        user.setId(userId);
        CreateRequest createRequest = new CreateRequest();
        Request request = new Request();
        request.setId(requestId);
        request.setCreator(user);
        when(requestRepo.findById(requestId)).thenReturn(Optional.of(request));

        assertThrows(ResponseStatusException.class, () -> requestService.editRequest(requestId, userId, createRequest));
    }

    @Test
    public void testEditRequest_NotDraft() {
        // Arrange
        long requestId = 1L;
        long userId = 1L;
        User user = new User();
        user.setId(userId);
        CreateRequest createRequest = new CreateRequest();
        Request request = new Request();
        request.setId(requestId);
        request.setCreator(user);
        request.setType(RequestType.SENT);
        when(requestRepo.findById(requestId)).thenReturn(Optional.of(request));

        assertThrows(ResponseStatusException.class, () -> requestService.editRequest(requestId, userId, createRequest));
    }
}
