package ru.mikhaylov.testlight.repos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.mikhaylov.testlight.domain.Request;
import ru.mikhaylov.testlight.domain.RequestType;


public interface RequestRepo extends JpaRepository<Request, Long> {

    Page<Request> findAllByCreatorId(Long userId, Pageable pageable);

    Page<Request> findByTypeInAndName(Pageable pageable, RequestType[] types, String name);

    Page<Request> findByTypeIn(Pageable pageable, RequestType[] types);
}
