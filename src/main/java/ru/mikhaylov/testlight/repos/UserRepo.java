package ru.mikhaylov.testlight.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.mikhaylov.testlight.domain.User;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);

    @Query(nativeQuery = true, value = "select refresh_token from user_refresh_token where user_id = :id")
    String getRefreshToken(@Param("id") long id);

    @Modifying
    @Query(nativeQuery = true, value = "delete from user_refresh_token where user_id = :id")
    void deleteAllById(@Param("id") long id);

    @Modifying
    @Query(nativeQuery = true, value = "insert into user_refresh_token values (:id, :token)")
    void setRefreshToken(@Param("id") long id, @Param("token") String token);
}
