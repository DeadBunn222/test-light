package ru.mikhaylov.testlight.dtos.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateRequest {

    private String name;
    private String phoneNumber;
    private String message;
}
