package ru.mikhaylov.testlight.dtos.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.mikhaylov.testlight.domain.RequestType;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class RequestResponse {

    private Long id;
    private String message;
    private String name;
    private String phoneNumber;
    private RequestType type;
    private UserResponse creator;
    private Date timeCreation;
}
