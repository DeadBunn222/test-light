package ru.mikhaylov.testlight.dtos.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.mikhaylov.testlight.domain.Role;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private String login;
    private String name;
    private Set<Role> roles;
}
