package ru.mikhaylov.testlight.dtos.responses;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ExceptionResponse {

    private String message;
    private Date timestamp;

    public ExceptionResponse(String message) {
        this.message = message;
        timestamp = new Date();
    }
}
