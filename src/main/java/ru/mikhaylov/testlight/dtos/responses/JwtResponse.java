package ru.mikhaylov.testlight.dtos.responses;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class JwtResponse {

    private TokenResponse accessToken;
    private TokenResponse refreshToken;
}
