package ru.mikhaylov.testlight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class TestLightApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestLightApplication.class, args);
	}

}
