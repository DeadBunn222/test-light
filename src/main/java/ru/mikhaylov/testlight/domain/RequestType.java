package ru.mikhaylov.testlight.domain;

public enum RequestType {
    DRAFT, SENT, ACCEPTED, DECLINED
}
