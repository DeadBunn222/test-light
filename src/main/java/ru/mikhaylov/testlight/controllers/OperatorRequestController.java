package ru.mikhaylov.testlight.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.mikhaylov.testlight.dtos.responses.RequestResponse;
import ru.mikhaylov.testlight.services.RequestService;

import java.util.List;

@RestController
@PreAuthorize("hasAuthority('OPERATOR')")
@RequestMapping("api/operator/requests")
public class OperatorRequestController {

    private final RequestService requestService;

    @Autowired
    public OperatorRequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/paging/{page}")
    public @ResponseBody List<RequestResponse> getResponses(
            @PathVariable int page,
            @RequestParam(required = false) Sort.Direction direction,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String creatorName
    ) {
        return requestService.getRequestsForOperator(page, direction, name, creatorName);
    }

    @PutMapping("accept/{id}")
    public @ResponseBody RequestResponse acceptRequest(
            @PathVariable Long id
    ) {
        return requestService.acceptRequest(id);
    }

    @PutMapping("decline/{id}")
    public @ResponseBody RequestResponse declineRequest(
            @PathVariable Long id
    ) {
        return requestService.declineRequest(id);
    }

    @GetMapping("{id}")
    public @ResponseBody RequestResponse getById(
            @PathVariable Long id
    ) {
        return requestService.getByIdForOperator(id);
    }
}
