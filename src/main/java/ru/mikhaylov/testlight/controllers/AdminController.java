package ru.mikhaylov.testlight.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.mikhaylov.testlight.dtos.responses.RequestResponse;
import ru.mikhaylov.testlight.dtos.responses.UserResponse;
import ru.mikhaylov.testlight.services.RequestService;
import ru.mikhaylov.testlight.services.UserService;

import java.util.List;

@RestController
@RequestMapping("api/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

    private final RequestService requestService;
    private final UserService userService;

    @Autowired
    public AdminController(RequestService requestService, UserService userService) {
        this.requestService = requestService;
        this.userService = userService;
    }

    @GetMapping("users")
    public @ResponseBody List<UserResponse> getUsers(
    ) {
        return userService.getUsersList();
    }

    @GetMapping("requests/paging/{page}")
    public @ResponseBody List<RequestResponse> getResponses(
            @PathVariable int page,
            @RequestParam(required = false) Sort.Direction direction,
            @RequestParam(required = false) String name
    ) {
        return requestService.getRequestsForAdmin(page, direction, name);
    }

    @PutMapping("set-operator")
    public @ResponseBody UserResponse setOperator(
            @RequestParam long id
    ) {
        return userService.setOperator(id);
    }
}
