package ru.mikhaylov.testlight.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mikhaylov.testlight.dtos.requests.AuthLoginRequest;
import ru.mikhaylov.testlight.dtos.responses.JwtResponse;
import ru.mikhaylov.testlight.jwt.JwtAuthentication;
import ru.mikhaylov.testlight.services.AuthService;

@RestController
@RequestMapping("api")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("auth/password")
    public @ResponseBody JwtResponse authByPassword(
            @RequestBody AuthLoginRequest request
    ) {
        return authService.authByPassword(request.getLogin(), request.getPassword());
    }

    @PostMapping("auth/refresh")
    public @ResponseBody JwtResponse authByRefresh(
            @RequestParam("token") String token
    ) {
        return authService.authByRefreshToken(token);
    }

    @DeleteMapping("logout")
    public ResponseEntity<Void> authByRefresh(JwtAuthentication jwtAuthentication) {
        authService.logout(jwtAuthentication.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
