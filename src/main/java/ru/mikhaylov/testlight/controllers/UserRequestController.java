package ru.mikhaylov.testlight.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.mikhaylov.testlight.dtos.requests.CreateRequest;
import ru.mikhaylov.testlight.dtos.responses.RequestResponse;
import ru.mikhaylov.testlight.jwt.JwtAuthentication;
import ru.mikhaylov.testlight.services.RequestService;

import java.util.List;

@RestController
@PreAuthorize("hasAuthority('USER')")
@RequestMapping("api/user/requests")
public class UserRequestController {

    private final RequestService requestService;

    @Autowired
    public UserRequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @PostMapping("create-and-send")
    public @ResponseBody RequestResponse createAndSendRequest(
            @RequestBody CreateRequest createRequest,
            JwtAuthentication jwtAuthentication
    ) {
        return requestService.createAndSendRequest(createRequest, jwtAuthentication.getUserId());
    }

    @PostMapping("create-draft")
    public @ResponseBody RequestResponse createDraftRequest(
            @RequestBody CreateRequest createRequest,
            JwtAuthentication jwtAuthentication
    ) {
        return requestService.createDraftRequest(createRequest, jwtAuthentication.getUserId());
    }

    @GetMapping("{page}")
    public @ResponseBody List<RequestResponse> getUserRequests(
            @PathVariable Integer page,
            @RequestParam(required = false) Sort.Direction direction,
            JwtAuthentication jwtAuthentication
    ) {
        return requestService.getUserRequests(jwtAuthentication.getUserId(), page, direction);
    }

    @PutMapping("edit/{id}")
    public @ResponseBody RequestResponse editRequest(
            @PathVariable Long id,
            @RequestBody CreateRequest createRequest,
            JwtAuthentication jwtAuthentication
    ) {
        return requestService.editRequest(id, jwtAuthentication.getUserId(), createRequest);
    }

    @PutMapping("send/{id}")
    public @ResponseBody RequestResponse sendRequest(
            @PathVariable Long id,
            JwtAuthentication jwtAuthentication
    ) {
        return requestService.sendRequest(id, jwtAuthentication.getUserId());
    }
}
