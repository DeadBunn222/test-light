package ru.mikhaylov.testlight.dadata;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(name = "dadata-client", url = "https://cleaner.dadata.ru/api/v1")
public interface DadataClient {
    @PostMapping(value = "/clean/phone", consumes = "application/json", produces = "application/json")
    List<DadataPhoneResponse> checkPhoneNumber(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("X-Secret") String secret,
            @RequestBody String[] phoneRequest
    );
}
