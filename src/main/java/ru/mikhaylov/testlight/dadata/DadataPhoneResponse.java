package ru.mikhaylov.testlight.dadata;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DadataPhoneResponse {
    private String country_code;
    private String city_code;
    private String phone;
    private String type;
}