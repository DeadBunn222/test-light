package ru.mikhaylov.testlight.dadata;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "dadata")
@Getter
@Setter
public class DadataProperties {
    private String apiKey;
    private String secretKey;
}

