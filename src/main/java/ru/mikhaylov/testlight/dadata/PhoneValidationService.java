package ru.mikhaylov.testlight.dadata;

import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PhoneValidationService {

    private final DadataClient dadataClient;
    private final DadataProperties dadataProperties;

    private final String ALLOWED_PHONE_NUMBER_TYPE = "Мобильный";

    @Autowired
    public PhoneValidationService(DadataClient dadataClient, DadataProperties dadataProperties) {
        this.dadataClient = dadataClient;
        this.dadataProperties = dadataProperties;
    }

    public String validatePhoneNumber(String phoneNumber) {
        String[] request = new String[]{phoneNumber};
        try {
            List<DadataPhoneResponse> result = dadataClient.checkPhoneNumber("Token " + dadataProperties.getApiKey(), dadataProperties.getSecretKey(), request);
            DadataPhoneResponse response = result.get(0);
            if (!response.getType().equals(ALLOWED_PHONE_NUMBER_TYPE)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Можно использовать только мобильный номер");
            }
            return response.getPhone();
        } catch (FeignException | NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ошибка проверки номера телефона", e);
        }
    }
}

