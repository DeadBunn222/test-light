package ru.mikhaylov.testlight.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;

@Configuration
public class CryptoConfig {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        List<String> list = Arrays.asList("user", "operator", "admin", "user-operator", "user-admin", "operator-admin", "user-operator-admin");
        for (String el : list) {
            System.out.println(el + " " + passwordEncoder.encode(el));
        }
    }
}
