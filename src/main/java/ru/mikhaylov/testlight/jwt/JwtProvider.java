package ru.mikhaylov.testlight.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.responses.TokenResponse;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

@Slf4j
@Component
public class JwtProvider {
    private final SecretKey jwtAccessSecret;
    private final SecretKey jwtRefreshSecret;
    private final int jwtAccessDurationSec;
    private final int jwtRefreshDurationDays;

    public JwtProvider(
            @Value("${jwt.secret.access}") String jwtAccessSecret,
            @Value("${jwt.secret.refresh}") String jwtRefreshSecret,
            @Value("${jwt.expires.access-sec}") int accessDuration,
            @Value("${jwt.expires.refresh-days}") int refreshDuration
    ) {
        this.jwtAccessSecret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtAccessSecret));
        this.jwtRefreshSecret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtRefreshSecret));
        this.jwtAccessDurationSec = accessDuration;
        this.jwtRefreshDurationDays = refreshDuration;
    }

    public TokenResponse generateAccessToken(User user) {
        Instant expiration = Instant.now().plus(jwtAccessDurationSec, ChronoUnit.SECONDS);
        String token = Jwts.builder()
                .subject(String.valueOf(user.getId()))
                .expiration(Date.from(expiration))
                .claim("roles", user.getRoles())
                .signWith(jwtAccessSecret)
                .compact();
        return new TokenResponse(token, expiration);
    }

    public TokenResponse generateRefreshToken(User user) {
        Instant expiration = Instant.now().plus(jwtRefreshDurationDays, ChronoUnit.DAYS);
        String token = Jwts.builder()
                .subject(String.valueOf(user.getId()))
                .expiration(Date.from(expiration))
                .signWith(jwtRefreshSecret)
                .compact();
        return new TokenResponse(token, expiration);
    }

    public boolean validateAccessToken(String accessToken) {
        return validateToken(accessToken, jwtAccessSecret);
    }

    public boolean validateRefreshToken(String refreshToken) {
        return validateToken(refreshToken, jwtRefreshSecret);
    }

    private boolean validateToken(String token, SecretKey secret) {
        try {
            Jwts.parser()
                    .verifyWith(secret)
                    .build()
                    .parseSignedClaims(token);
            return true;

        } catch (SignatureException e) {
            log.warn("Invalid token signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.warn("Invalid token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.warn("Token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.warn("Token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.warn("Token claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    public Claims getAccessClaims(String accessToken) {
        return getClaims(accessToken, jwtAccessSecret);
    }

    public Claims getRefreshClaims(String refreshToken) {
        return getClaims(refreshToken, jwtRefreshSecret);
    }

    private Claims getClaims(String token, SecretKey secret) {
        return Jwts.parser()
                .verifyWith(secret)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }
}
