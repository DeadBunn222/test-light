package ru.mikhaylov.testlight.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.dadata.PhoneValidationService;
import ru.mikhaylov.testlight.domain.Request;
import ru.mikhaylov.testlight.domain.RequestType;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.requests.CreateRequest;
import ru.mikhaylov.testlight.dtos.responses.RequestResponse;
import ru.mikhaylov.testlight.repos.RequestRepo;

import java.util.List;

@Service
public class RequestService {

    private final UserService userService;
    private final RequestRepo requestRepo;
    private final ModelMapper modelMapper;
    private final PhoneValidationService phoneValidationService;

    @Autowired
    public RequestService(
            UserService userService,
            RequestRepo requestRepo,
            ModelMapper mapper, PhoneValidationService phoneValidationService
    ) {
        this.userService = userService;
        this.requestRepo = requestRepo;
        this.modelMapper = mapper;
        this.phoneValidationService = phoneValidationService;
    }

    public Request findById(long id) {
        return requestRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Заявка не найдена"));
    }

    public RequestResponse createAndSendRequest(CreateRequest createRequest, long userId) {
        return createRequest(createRequest, userId, RequestType.SENT);
    }

    public RequestResponse createDraftRequest(CreateRequest createRequest, long userId) {
        return createRequest(createRequest, userId, RequestType.DRAFT);
    }

    private RequestResponse createRequest(CreateRequest createRequest, long userId, RequestType type) {
        User creator = userService.findById(userId);

        String phoneNumber = phoneValidationService.validatePhoneNumber(createRequest.getPhoneNumber());

        Request request = new Request();
        modelMapper.map(createRequest, request);
        request.setCreator(creator);
        request.setType(type);
        request.setPhoneNumber(phoneNumber);

        requestRepo.save(request);

        return modelMapper.map(request, RequestResponse.class);
    }

    public List<RequestResponse> getUserRequests(Long userId, int pageNumber, Sort.Direction direction) {
        return requestRepo.findAllByCreatorId(userId, getPageable(direction, pageNumber))
                .stream()
                .map(it -> modelMapper.map(it, RequestResponse.class))
                .toList();
    }

    public RequestResponse editRequest(long requestId, long userId, CreateRequest createRequest) {

        Request editedRequest = findById(requestId);

        if (editedRequest.getCreator().getId() != userId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Редактировать заявку может только создатель");
        }

        if (editedRequest.getType() != RequestType.DRAFT) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Редактировать можно только черновики");
        }

        String phoneNumber = createRequest.getPhoneNumber();
        if (!editedRequest.getPhoneNumber().equals(createRequest.getPhoneNumber())) {
            phoneNumber = phoneValidationService.validatePhoneNumber(createRequest.getPhoneNumber());
        }

        modelMapper.map(createRequest, editedRequest);
        editedRequest.setPhoneNumber(phoneNumber);
        requestRepo.save(editedRequest);

        return modelMapper.map(editedRequest, RequestResponse.class);
    }

    public RequestResponse sendRequest(long requestId, long userId) {

        Request sentRequest = findById(requestId);

        if (sentRequest.getCreator().getId() != userId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Отправлять заявку может только создатель");
        }

        if (sentRequest.getType() != RequestType.DRAFT) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Отправлять можно только черновики");
        }

        sentRequest.setType(RequestType.SENT);
        requestRepo.save(sentRequest);

        return modelMapper.map(sentRequest, RequestResponse.class);
    }

    public RequestResponse acceptRequest(long id) {
        return acceptOrDecline(id, RequestType.ACCEPTED);
    }

    public RequestResponse declineRequest(long id) {
        return acceptOrDecline(id, RequestType.DECLINED);
    }

    private RequestResponse acceptOrDecline(long requestId, RequestType type) {
        Request request = findById(requestId);

        if (request.getType() != RequestType.SENT) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Принять/Отклонить можно только отправленные заявки");
        }

        request.setType(type);
        requestRepo.save(request);

        return modelMapper.map(request, RequestResponse.class);
    }

    public RequestResponse getByIdForOperator(long id) {
        Request request = findById(id);

        if (request.getType() != RequestType.SENT) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Смотреть можно только отправленные заявки");
        }

        return modelMapper.map(request, RequestResponse.class);
    }

    public List<RequestResponse> getRequestsForOperator(int pageNumber, Sort.Direction direction, String name, String creatorName) {

        Request requestExample = new Request();
        requestExample.setType(RequestType.SENT);
        requestExample.setName(name);

        User user = new User();
        user.setName(creatorName);
        requestExample.setCreator(user);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreNullValues()
                .withIgnorePaths("timeCreation")
                .withMatcher("creator.name", ExampleMatcher.GenericPropertyMatcher::contains);

        Example<Request> example = Example.of(requestExample, matcher);

        return requestRepo.findAll(example, getPageable(direction, pageNumber))
                .stream()
                .map(it -> modelMapper.map(it, RequestResponse.class))
                .toList();
    }

    public List<RequestResponse> getRequestsForAdmin(int pageNumber, Sort.Direction direction, String name) {

        RequestType[] allowedTypes = new RequestType[]{RequestType.SENT, RequestType.ACCEPTED, RequestType.DECLINED};

        Page<Request> result =
                name == null
                        ? requestRepo.findByTypeIn(getPageable(direction, pageNumber), allowedTypes)
                        : requestRepo.findByTypeInAndName(getPageable(direction, pageNumber), allowedTypes, name);

        return result
                .stream()
                .map(it -> modelMapper.map(it, RequestResponse.class))
                .toList();
    }

    private Pageable getPageable(Sort.Direction direction, int pageNumber) {
        int DEFAULT_PAGE_SIZE = 5;
        String SORT_FIELD = "timeCreation";
        return direction == null
                ? PageRequest.of(pageNumber, DEFAULT_PAGE_SIZE)
                : PageRequest.of(pageNumber, DEFAULT_PAGE_SIZE, direction, SORT_FIELD);
    }
}
