package ru.mikhaylov.testlight.services;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.responses.JwtResponse;
import ru.mikhaylov.testlight.dtos.responses.TokenResponse;
import ru.mikhaylov.testlight.jwt.JwtProvider;
import ru.mikhaylov.testlight.repos.UserRepo;

import java.util.Objects;

@Service
public class AuthService {
    private final JwtProvider jwtProvider;
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    private final String PASSWORD_ERROR = "Неправильный логин или пароль";
    private final String REFRESH_ERROR = "Ошибка авторизации через рефреш токен";

    @Autowired
    public AuthService(
            JwtProvider jwtProvider,
            UserRepo userRepo,
            PasswordEncoder passwordEncoder
    ) {
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
        this.userRepo = userRepo;
    }

    @Transactional
    public JwtResponse authByPassword(String login, String password) {
        User user = userRepo.findByLogin(login)
                .orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, PASSWORD_ERROR)
                );

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, PASSWORD_ERROR);
        }

        return response(user);
    }

    @Transactional
    public JwtResponse authByRefreshToken(String refreshToken) {

        if (!jwtProvider.validateRefreshToken(refreshToken)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, REFRESH_ERROR);
        }

        long id = Long.parseLong(jwtProvider.getRefreshClaims(refreshToken)
                .getSubject());

        String tokenInDb = userRepo.getRefreshToken(id);

        if (tokenInDb == null || !Objects.equals(refreshToken, tokenInDb)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, REFRESH_ERROR);
        }

        User user = userRepo.findById(id)
                .orElseThrow(
                        () -> new RuntimeException(REFRESH_ERROR)
                );

        return response(user);
    }

    @Transactional
    private JwtResponse response(User user) {
        TokenResponse refreshToken = jwtProvider.generateRefreshToken(user);
        userRepo.deleteAllById(user.getId());
        userRepo.setRefreshToken(user.getId(), refreshToken.getToken());

        JwtResponse response = new JwtResponse();
        response.setAccessToken(jwtProvider.generateAccessToken(user));
        response.setRefreshToken(refreshToken);

        return response;
    }

    @Transactional
    public void logout(long id) {
        userRepo.deleteAllById(id);
    }
}
