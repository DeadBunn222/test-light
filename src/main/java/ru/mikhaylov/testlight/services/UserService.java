package ru.mikhaylov.testlight.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.mikhaylov.testlight.domain.Role;
import ru.mikhaylov.testlight.domain.User;
import ru.mikhaylov.testlight.dtos.responses.UserResponse;
import ru.mikhaylov.testlight.repos.UserRepo;

import java.util.List;

@Service
public class UserService {

    private final UserRepo userRepo;
    private final ModelMapper modelMapper;

    @Autowired
    public UserService(
            UserRepo userRepo,
            ModelMapper modelMapper
    ) {
        this.userRepo = userRepo;
        this.modelMapper = modelMapper;
    }

    public User findById(long id) {
        return userRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден"));
    }

    public List<UserResponse> getUsersList() {
        return userRepo.findAll()
                .stream()
                .map(it -> modelMapper.map(it, UserResponse.class))
                .toList();
    }

    public UserResponse setOperator(long id) {
        User user = findById(id);
        user.getRoles().add(Role.OPERATOR);
        userRepo.save(user);

        return modelMapper.map(user, UserResponse.class);
    }
}
